# SKA LOW-CSP Jupyter Notebooks

This repository contains Jupyter notebooks used to develop tests for LOW-CSP.

## Running on Digital Signal PSI BinderHub

[![Launch Binder on Digital Signal PSI][binder-badge]][launch-binder]

Note: Requires VPN access, instructions can be found [here][vpn-access]

## Useful information

* You can inspect the Tango device Attributes via [Taranta][taranta]
* You can inspect which jupyterhub servers you have running by going [here][jupyterhub]

[binder-badge]: https://raw.githubusercontent.com/choldgraf/binderhub/0df2ffc7901dc1047e71ade456d1126ae93b4c00/binderhub/static/images/badge_logo.svg
[vpn-access]: https://developer.skao.int/projects/ska-low-csp/en/latest/testing/clp-interactive-sessions.html#setup-a-vpn-connection
[launch-binder]: http://k8s.clp.skao.int/binderhub/v2/gl/ska-telescope%2Fska-low-csp-notebooks/HEAD
[jupyterhub]: http://k8s.clp.skao.int/binderhub/jupyterhub/hub/home
[taranta]: http://k8s.clp.skao.int/ska-low-csp-integration/taranta/devices
